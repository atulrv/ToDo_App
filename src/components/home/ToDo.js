import React, {PropTypes} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as uiAction from "../../actions/uiAction";
import Header from '../common/header';

export default class ToDoApp extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.toDoItem = [];
        this.onchangeTextItem = '';
        this.showInput = false;

    }

    componentDidMount() {

    }

    showInputField = () => {
        this.showInput = true;
        console.log("this.showInput", this.showInput);
        this.forceUpdate();
    };

    hideInputField = () => {
        this.showInput = false;
        console.log("this.showInput", this.showInput);
        this.forceUpdate();

    };

    onChangeText = (event) => {
        this.onchangeTextItem = {item: event.target.value};
        this.forceUpdate();
        //console.log("this.onchangeTextItem", this.onchangeTextItem)

    };

    saveItem = () => {
        this.toDoItem.push(this.onchangeTextItem);
        this.forceUpdate();
        //console.log("this.toDoItem", this.toDoItem);
    };

    deleteItem = (i) => {
        for(let j = 0; j< this.toDoItem.length;j++){
            if(i === j){
               console.log(i,j);
               let indexItem = j;
               console.log("indexItem",indexItem)
                //this.toDoItem.splice(this.toDoItem[i]);*/
                if(indexItem > -1){
                    this.toDoItem.splice(indexItem,1);

                }
            //   console.log("this.toDoItem.indexOf(indexItem)",this.toDoItem.indexOf(indexItem))

            }
        }
        //console.log("item",this.toDoItem)
        this.forceUpdate();

    };

    render() {
        return (
            <div>
                <Header/>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-4"></div>
                        <div className="col-md-4">
                            {
                                this.showInput === false ?
                                    <button type="button" className="btn btn-success btn-circle btn-xl text-center"
                                            onClick={this.showInputField}><i className="glyphicon glyphicon-ok"></i>
                                    </button>
                                    :
                                    <div>
                                        <h3 className="text-center">Please Add ! <i
                                            className="glyphicon glyphicon-ok"></i></h3>
                                        <div>
                                            <input type="text" className="form-control form-rounded"
                                                   onChange={this.onChangeText} placeholder="Add New ToDo here.....!"/>
                                        </div>
                                        <div>
                                            {
                                                this.onchangeTextItem.length === 0 || this.onchangeTextItem.item === '' ?
                                                    <button type="button"
                                                            className="btn btn-primary mar10 btn-round-sm btn-sm"
                                                            onClick={this.hideInputField}>Cancel</button>
                                                    :
                                                    <button type="button"
                                                            className="btn btn-success mar10 btn-round-sm btn-sm"
                                                            onClick={this.saveItem}>Add</button>

                                            }
                                            {/*<button type="button" className="btn btn-success  .btn-circle.btn-lg" onClick={this.hideInputField}><i className="glyphicon glyphicon-remove"></i></button>*/}
                                        </div>
                                    </div>
                            }

                            <div>
                                {
                                    this.toDoItem.map((d, i) => {
                                        //console.log("MAP", d, i)
                                        return (

                                                <ul className="h3">
                                                    <div className="col-md-3">
                                                        {d.item}
                                                    </div>
                                                    <div>
                                                        <button type="button" className=" btn btn-danger"
                                                                onClick={() => this.deleteItem(i)} ><i className="glyphicon glyphicon-remove"></i>
                                                        </button>
                                                    </div>

                                                </ul>

                                        )
                                    })

                                }
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

ToDoApp.propTypes = {
    uiAction: PropTypes.object
};

function mapStateToProps(state, ownProps) {
    //return {state: {currentUser: {}}};
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        uiAction: bindActionCreators(uiAction, dispatch)

    };
}

