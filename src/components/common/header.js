import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import * as uiAction from "../../actions/uiAction";
import {bindActionCreators} from 'redux';

class Header extends React.Component {

    constructor(props, context) {
        super(props, context);

    }

    render() {
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">WebSiteName</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li className="active"><a href="#">Home</a></li>
                        <li><a href="#">Page 1</a></li>
                        <li><a href="#">Page 2</a></li>
                    </ul>
                    <form className="navbar-form navbar-left">
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Search"/>
                        </div>
                        <button type="submit" className="btn btn-default">Submit</button>
                    </form>
                </div>
            </nav>
    );
   }
}

    Header.propTypes = {
        uiActions: PropTypes.object
    };

    function mapStateToProps(state, ownProps) {
        return state.application;
    }

    function mapDispatchToProps(dispatch) {
        return {
        uiActions: bindActionCreators(uiAction, dispatch)
    };
    }

    export default connect(mapStateToProps, mapDispatchToProps)(Header);
