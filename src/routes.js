import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/app';
import ToDoApp from './components/home/ToDo';

export default (
<Route path="/" component={App}>
    <IndexRoute component={ToDoApp}/>
      {/*<path path="addFormforOrg" component={OrganizationForm}/>*/}
    </Route>
);